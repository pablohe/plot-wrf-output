#!/usr/bin/env python

def plot_wrf_output(args):
  """

  This software allow ploting WRF version 3.6 output netcdf files  (http://www2.mmm.ucar.edu/wrf/users/)
  use 
  *NetCDF4-python
  *matplotlib
  *mpl_toolkits.basemap


  * 1.0

  At the moment plot map one 3D variable in level 0, you  ca

  """
  import sys,getopt
  #from netCDF4 import Dataset
  from Scientific.IO.NetCDF import NetCDFFile as Dataset
  import matplotlib
  matplotlib.use('agg')
  import matplotlib.pyplot as plt
  import numpy as np
  import os
  from datetime import datetime, timedelta
  #import coltbls as coltbls
  from mpl_toolkits.basemap import Basemap
  from matplotlib.colors import LinearSegmentedColormap
  from mpl_toolkits.axes_grid import make_axes_locatable
  import matplotlib.axes as maxes

  filename=args.fileInput

  nc = Dataset(filename, 'r')

  x_dim = nc.dimensions['west_east']
  y_dim = nc.dimensions['south_north']

  # Get the grid spacing
  dx = float(nc.DX)
  dy = float(nc.DY)
  width_meters = dx * (x_dim - 1)
  height_meters = dy * (y_dim - 1)
  cen_lat = float(nc.CEN_LAT)
  cen_lon = float(nc.CEN_LON)
  truelat1 = float(nc.TRUELAT1)
  truelat2 = float(nc.TRUELAT2)
  standlon = float(nc.STAND_LON)

  m = Basemap(resolution='i',projection='lcc',\
      width=width_meters,height=height_meters,\
      lat_0=cen_lat,lon_0=cen_lon,lat_1=truelat1,\
      lat_2=truelat2)

  # This sets the standard grid point structure at full resolution
  x,y = m(nc.variables['XLONG'][0],nc.variables['XLAT'][0])

  # This sets a thinn-ed out grid point structure for plotting
  # wind barbs at the interval specified in "thin"
  #x_th,y_th = m(nc.variables['XLONG'][0,::thin,::thin],\
  #    nc.variables['XLAT'][0,::thin,::thin])

  # Set universal figure margins
  width = 10
  height = 8

  plt.figure(figsize=(width,height))
  plt.rc("figure.subplot", left = .001)
  plt.rc("figure.subplot", right = .999)
  plt.rc("figure.subplot", bottom = .001)
  plt.rc("figure.subplot", top = .999)


  m.drawstates(color='k', linewidth=1.25)
  m.drawstates(color='k', linewidth=1.25)
  m.drawcoastlines(color='k')
  m.drawcountries(color='g', linewidth=1.25)
  #:m.fillcontinents(color='white',lake_color='blue')


  m.drawparallels(np.arange(-70,65,20),labels=np.ones(7),fontsize=8)
  m.drawmeridians(np.arange(-180,180,20),labels=np.ones(36),fontsize=8)

  var=nc.variables['U'].getValue()
  m.pcolor(x,y,var[0,1,:,1:])

  plt.savefig(args.fileOutput)


  #nc.variables['RAINNC'][0]
  #m.pcolor(x,y,rainc)


  
if __name__ == "__main__":


  import argparse

  parser = argparse.ArgumentParser()

  parser.add_argument('fileInput')
  parser.add_argument('fileOutput',  help="support png, pdf, ps, eps and svg. Like matplotlib.pyplot.savefig")
#  parser.add_argument('variable2D')
#  parser.add_argument('variable3D')
#  parser.add_argument('level')
#  
  
  args = parser.parse_args()


  plot_wrf_output(args)


