# README #

Ploting WRF  (http://www2.mmm.ucar.edu/wrf/users/) output 

In wrfout file
out  png, pdf, ps, eps and svg

Dependencies:

* NetCDF4-python
* matplotlib
* mpl_toolkits.basemap

Version * 1.0.0

At the moment plot map with one 3D variable U component of wind.


# How to use: #

./plot_wrf_out.py ./wrfout_d01_2015-01-13_00\:00\:00 ./out_map.png